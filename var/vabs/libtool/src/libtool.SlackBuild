#!/bin/sh

# Copyright 2008, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="libtool"
VERSION=${VERSION:-"2.4"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILDNUM=${BUILD:-2}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
NUMJOBS=${NUMJOBS:--j6}
ARCH=${ARCH:-"$(uname -m)"}
LINK=${LINK:-"http://ftp.gnu.org/gnu/${NAME}/${NAME}-${VERSION}.tar.xz"}


# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

if [ "$NORUN" != 1 ]; then

CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-libtool
RELEASEDIR=$CWD/..

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf libtool-$VERSION

# Download the source code
for src in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $src
	)
done

tar xvf $CWD/libtool-$VERSION.tar.xz || exit 1

cd libtool-$VERSION
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

zcat $CWD/libtool.no.moved.warning.diff.gz | patch -p1 --verbose || exit 1 

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --mandir=/usr/man \
  --docdir=/usr/doc/libtool-$VERSION \
  --build=$ARCH-slackware-linux || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

gzip -9 $PKG/usr/man/man?/*.?

rm -f $PKG/usr/info/dir
gzip -9 $PKG/usr/info/*

mkdir -p $PKG/usr/doc/libtool-$VERSION
cp -a \
  AUTHORS COPYING* NEWS README* THANKS TODO \
    $PKG/usr/doc/libtool-$VERSION

mkdir $PKG/install
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG

echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
cat $CWD/slack-desc > $RELEASEDIR/slack-desc
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
/sbin/makepkg -l y -c n $RELEASEDIR/libtool-${VERSION}-$ARCH-$BUILD.txz
fi
