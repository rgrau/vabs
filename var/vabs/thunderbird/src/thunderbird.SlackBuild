#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .tlz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
#
# This Template was compiled from the contributions of many users of the Vector
# Linux forum at http://forum.vectorlinux.com and from tidbits collected 
# from all over the internet. 
#
# Generated by sbbuilder-0.4.12.3, written by Rodrigo Bistolfi 
# (rbistolfi) and Raimon Grau Cuscó (Kidd) for VectorLinux.
#
# Please put your name below if you add some original scripting lines.
# AUTHORS = 

NAME="thunderbird"            #Enter package Name!
VERSION=${VERSION:-"14.0"}      #Enter package Version!
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"hata_ph"}   #Enter your Name!
#LINK=${LINK:-"http://releases.mozilla.org/pub/mozilla.org/$NAME/releases/$VERSION/source/$NAME-$VERSION.source.tar.bz2"}
LINK=${LINK:-"ftp://ftp.mozilla.org/pub/$NAME/releases/$VERSION/source/$NAME-$VERSION.source.tar.bz2"}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"pixman"} #Add make deps for packaging
#----------------------------------------------------------------------------

if [ "$NORUN" != 1 ]; then


#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------



if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i486-slackware-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-slackware-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------


#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------


rm -rf $PKG
mkdir -p $PKG
cd $TMP $PKG/usr/lib${LIBDIRSUFFIX}
rm -rf comm-release


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xvf $CWD/$NAME-$VERSION.source.tar.* || exit 1
#this moves whatever was extracted to the std dirname we are expecting
mv * $NAME-$VERSION &> /dev/null 2>&1
mkdir -p $PKG
#-----------------------------------------------------


cd $TMP/comm-release

# Put any Patches into a patches folder in the src dir
#-----------------------------------------------------
for i in $CWD/patches/*;do
#  patch -p1 <$i
  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
done
#-----------------------------------------------------

# Fix a long standing bug that's prevented staying current on GTK+.
# Thanks to the BLFS folks.  :-)
cat << EOF >> mozilla/layout/build/Makefile.in

ifdef MOZ_ENABLE_CANVAS
EXTRA_DSO_LDOPTS += \$(XLDFLAGS) -lX11 -lXrender
endif

EOF

if gcc --version | grep -q "gcc (GCC) 4.7.0" ; then
 # Enable compiling with gcc-4.7.0:
 sed -i '/fcntl.h/a#include <unistd.h>' \
   mozilla/ipc/chromium/src/base/{file_util_linux,message_pump_libevent,process_util_posix}.cc &&
 sed -i '/sys\/time\.h/a#include <unistd.h>' mozilla/ipc/chromium/src/base/time_posix.cc &&
 sed -i 's#\"PRIxPTR#\" PRIxPTR#g' mozilla/layout/base/tests/TestPoisonArea.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/base/search/src/nsMsgSearchAdapter.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/base/src/nsMsgFolderCompactor.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/compose/src/nsSmtpProtocol.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/imap/src/nsImapMailFolder.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/imap/src/nsImapProtocol.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/imap/src/nsImapServerResponseParser.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/local/src/nsPop3Protocol.cpp &&
 sed -i 's#\"CRLF#\" CRLF#g' mailnews/mime/src/mimedrft.cpp &&
 sed -i 's#\"MSG_LINEBREAK#\" MSG_LINEBREAK#g' mailnews/mime/src/mimemult.cpp &&
 sed -i 's#\"MSG_LINEBREAK#\" MSG_LINEBREAK#g' mailnews/base/src/nsMsgFolderCompactor.cpp &&
 sed -i 's# ""##' mozilla/browser/base/Makefile.in
fi

# Work around a glitch where .py files go missing.
# Don't apply this to future versions...  we'll see how those go then.
#if [ "$VERSION" = "13.0" ]; then
zcat $CWD/patches/thunderbird_cache_dir.patch.gz | patch -p1 --verbose || exit 1
#fi

#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

echo "Configuring source..."
export MOZILLA_DIR=$TMP/comm-release/mozilla &&
export MOZILLA_OFFICIAL="1" &&
export BUILD_OFFICIAL="1" &&
export MOZ_PHOENIX="1" &&
./configure --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --with-included-gettext \
  --enable-official-branding \
  --with-default-mozilla-five-home=/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION \
  --with-system-zlib \
  --enable-application=mail \
  --enable-default-toolkit=cairo-gtk2 \
  --enable-system-pixman \
  --enable-startup-notification \
  --enable-crypto \
  --enable-ldap \
  --enable-libxul \
  --enable-svg \
  --enable-canvas \
  --enable-xft \
  --enable-xinerama \
  --enable-optimize \
  --enable-reorder \
  --enable-strip \
  --enable-cpp-rtti \
  --enable-single-profile \
  --disable-accessibility \
  --disable-crashreporter \
  --disable-debug \
  --disable-tests \
  --disable-logging \
  --disable-pedantic \
  --disable-installer \
  --disable-profilesharing \
  --program-prefix="" \
  --program-suffix="" \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1
  # Complains about missing APNG support in Slackware's libpng:
  #--with-system-png \

#make clean || # make clean cause error mkdir
make || exit 1

make install DESTDIR=$PKG || exit


#######################################################################
#Miscellenious tweaks and things outside a normal ./configure go here #
#######################################################################

# create firefox symlink
ln -s /usr/lib${LIBDIRSUFFIX}/$NAME-$VERSION $PKG/usr/lib${LIBDIRSUFFIX}/$NAME

# We don't need these (just symlinks anyway):
rm -rf $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-devel-$VERSION

# Nor these:
rm -rf $PKG/usr/include

# Thunderbird 3.x cruft?
# If we still need something like this (and you know what we need :), let me know.
#( cd $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION
#  cp -a defaults/profile/mimeTypes.rdf defaults/profile/mimeTypes.rdf.orig
#  zcat $CWD/mimeTypes.rdf > defaults/profile/mimeTypes.rdf || exit 1
#) || exit 1

mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}/mozilla/plugins
mkdir -p $PKG/usr/share/applications
cat $CWD/mozilla-thunderbird.desktop > $PKG/usr/share/applications/mozilla-thunderbird.desktop
mkdir -p $PKG/usr/share/pixmaps
cat $CWD/thunderbird.png > $PKG/usr/share/pixmaps/thunderbird.png

# These files/directories are usually created if Firefox is run as root,
# which on many systems might (and possibly should) be never.  Therefore, if we
# don't see them we'll put stubs in place to prevent startup errors.
( cd $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION
  if [ -d extensions/talkback\@mozilla.org ]; then
    if [ ! -r extensions/talkback\@mozilla.org/chrome.manifest ]; then
      echo > extensions/talkback\@mozilla.org/chrome.manifest
    fi
  fi
  if [ ! -d updates ]; then
    mkdir -p updates/0
  fi
)

# Need some default icons in the right place:
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION/chrome/icons/default
install -m 644 other-licenses/branding/thunderbird/default16.png \
  $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION/icons/
install -m 644 other-licenses/branding/thunderbird/default16.png \
  $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION/chrome/icons/default/ 
( cd $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION
  install -m 644 icons/{default,mozicon50}.xpm chrome/icons/default/
)

# Copy over the LICENSE
#install -p -c -m 644 LICENSE $PKG/usr/lib${LIBDIRSUFFIX}/thunderbird-$VERSION/

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a LEGAL LICENSE README* $PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ] ; then
mkdir -p $PKG/usr/man
mv $PKG/usr/share/man/* $PKG/usr/man
rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;

if [ -d $PKG/usr/share/info ] ; then
mkdir -p $PKG/usr/info
mv $PKG/usr/share/info/* $PKG/usr/info
rm -rf $PKG/usr/share/info
fi 

find $PKG/usr/info -type f -exec gzip -9 {} \;
mkdir -p $PKG/install
if [ -d $PKG/usr/info ] ; then
cat >> $PKG/install/doinst.sh << EOF
CWD=\$(pwd)
cd usr/info
if [ -f dir ];then
    rm dir
fi
if [ -f dir.gz ];then
    rm dir.gz
fi
for i in *.info.gz;do
        install-info \$i dir
done
cd \$CWD
EOF
fi

# Add schemas install to the doinst.sh if schemas are found.
[ -d $PKG/etc/gconf/schemas ] && {
# Make sure we have gconftool installed
echo "if [ -x usr/bin/gconftool-2 ]; then" >> $PKG/install/doinst.sh
( cd $PKG/etc/gconf/schemas
for schema in *.schemas; do
 # Install schemas
 echo "GCONF_CONFIG_SOURCE=\"xml::etc/gconf/gconf.xml.defaults\" \
   usr/bin/gconftool-2 --makefile-install-rule \
   etc/gconf/schemas/${schema} >/dev/null 2>&1" \
   >> $PKG/install/doinst.sh
done;
)
# Finish off gconf block
echo "fi" >> $PKG/install/doinst.sh
}

mkdir -p $PKG/install
cat >> $PKG/install/doinst.sh << EOF
# update rarian database
if [ -x usr/bin/rarian-sk-update ]; then
  usr/bin/rarian-sk-update 1> /dev/null 2> /dev/null
fi

# update mime database
if [ -x usr/bin/update-mime-database ]; then
  usr/bin/update-mime-database usr/share/mime 1> /dev/null 2> /dev/null
fi

# update desktop entries
if [ -x usr/bin/update-desktop-database ]; then
  usr/bin/update-desktop-database 1> /dev/null 2> /dev/null
fi

# update hicolor icons
if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
	rm -f usr/share/icons/hicolor/icon-theme.cache
fi
usr/bin/gtk-update-icon-cache -f -q usr/share/icons/hicolor 1>/dev/null 2>/dev/null

if [ -x /usr/bin/glib-compile-schemas ]; then
  /usr/bin/glib-compile-schemas usr/share/glib-2.0/schemas/ >/dev/null 2>&1
fi

# Restart gconfd-2 if running to reload new gconf settings
if ps acx | grep -q gconfd-2 ; then
        killall -HUP gconfd-2 ;
fi
EOF

#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "$NAME")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 $NAME: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
$NAME: $NAME (Mozilla Thunderbird mail application)
$NAME:
$NAME: Mozilla Thunderbird is a redesign of the Mozilla mail component
$NAME: written using the XUL user interface language.  Thunderbird makes
$NAME: emailing safer, faster, and easier than ever before with the
$NAME: industry's best implementations of features such as intelligent spam
$NAME: filters, built-in RSS reader, quick search, and much more.
$NAME:
$NAME: License: GPL
$NAME: Authors: http://www.mozilla.org/projects/thunderbird/
$NAME: Website: http://www.mozilla.org/projects/thunderbird/

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\.\/configure\ /" $TMP/$NAME-$VERSION/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
#--------------------------------------------------------------
fi
# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
