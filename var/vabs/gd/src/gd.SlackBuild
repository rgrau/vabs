#!/bin/sh

# Copyright 2006, 2007, 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME="gd"
VERSION=${VERSION:-"2.0.35"}
BUILDNUM=${BUILDNUM:-"4"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""}
LINK=${LINK:-"http://google-desktop-for-linux-mirror.googlecode.com/files/${NAME}-${VERSION}.tar.gz"}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j7 "}

CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-${NAME}
RELEASEDIR=${RELEASEDIR:-$CWD/..}

if [ "$NORUN" != 1 ]; then

	for link in $(echo $LINK); do
		(
		cd $CWD
		wget -c --no-check-certificate $link
		)
	done

PKGNAM=${NAME}
rm -rf $PKG
mkdir -p $TMP $PKG

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

cd $TMP
rm -rf ${PKGNAM}-${VERSION}
tar xvf $CWD/${PKGNAM}-$VERSION.tar.?z* || exit 1
mv gd $NAME-$VERSION
cd ${PKGNAM}-$VERSION/$VERSION

zcat $CWD/gd.png14.diff.gz | patch -p1 --verbose || exit 1

# Make sure ownerships and permissions are sane:
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# Add pkgconfig file:
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}/pkgconfig
cat $CWD/gdlib.pc | sed -e "s/@VERSION@/$VERSION/" \
  | sed -e "s#/lib#/lib${LIBDIRSUFFIX}#g" \
  > $PKG/usr/lib${LIBDIRSUFFIX}/pkgconfig/gdlib.pc

# Configure:
CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --disable-static \
  --program-prefix= \
  --program-suffix= \
  --build=$ARCH-slackware-linux || exit 1

# Build and install:
make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory:
mkdir -p $PKG/usr/doc/${PKGNAM}-$VERSION
cp -a \
  COPYING INSTALL README-JPEG.TXT README.TXT index.html \
  $PKG/usr/doc/${PKGNAM}-$VERSION
# Fix stupid permissions:
chmod 644 $PKG/usr/doc/${PKGNAM}-$VERSION/*

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $RELEASEDIR/slack-desc

cd $TMP/package-${PKGNAM}
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $RELEASEDIR/${PKGNAM}-$VERSION-$ARCH-$BUILD.txz

fi
