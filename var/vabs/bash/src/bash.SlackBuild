#!/bin/sh

# Copyright 2005-2012  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Modified 2011, 2012  by Eric Hameleers <alien at slackware.com> for ARM port.


# Set initial variables:
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-bash
NAME="bash"
VERSION=${VERSION:-4.2}
BUILD=${BUILD:-2vl70}
LINK=${LINK:-"ftp://ftp.gnu.org/gnu/$NAME/$NAME-$VERSION.tar.gz"}

if [ "$NORUN" != 1 ]; then

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

if [ "$ARCH" = "i386" ]; then
  SLKCFLAGS="-O2 -march=i386 -mcpu=i686"
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
elif [ "$ARCH" = "armv7hl" ]; then
  SLKCFLAGS="-O2 -march=armv7-a -mfpu=vfpv3-d16"
else
  SLKCFLAGS="-O2"
fi

case "$ARCH" in
    arm*) TARGET=$ARCH-slackware-linux-gnueabi ;;
    *)    TARGET=$ARCH-slackware-linux ;;
esac

if [ ! -d $TMP ]; then
  mkdir -p $TMP # location to build the source
fi
rm -rf $PKG
mkdir -p $PKG

# Determine bash patchlevel:
PATCHLEVEL="$( cd $CWD/bash-${VERSION}-patches ; /bin/ls bash4?-??? | tail -1 | cut -f 2 -d - 2> /dev/null )"
if [ "$PATCHLEVEL" = "" ]; then
  PATCHLEVEL=0
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
cd $TMP
rm -rf bash-$VERSION
tar xvf $CWD/bash-$VERSION.tar.?z* || exit 1
cd bash-$VERSION || exit 1
chown -R root:root .
find . -perm 664 | xargs chmod 644
find . -perm 775 | xargs chmod 755

if [ -d $CWD/bash-${VERSION}-patches ]; then
  ( cd $CWD/bash-${VERSION}-patches ; cat bash4?-??? ) | patch -p0 --verbose || exit 1
fi

# End of preparations
if echo "$*" | grep -qw -- --prep ; then
  exit 0
fi

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --build=$TARGET

make -j4 || make || exit 1
make install DESTDIR=$PKG
mv $PKG/usr/share/doc $PKG/usr
mkdir -p $PKG/bin
mv $PKG/usr/bin/bash $PKG/bin/bash4.new
# We don't include the "bashbug" script.
rm -rf $PKG/usr/bin $PKG/usr/man/man1/bashbug.1

# Strip binaries:
( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# bash.1 is already installed by "make install"
( cd doc
  for page in builtins.1 rbash.1 ; do
    cat $page | gzip -9c > $PKG/usr/man/man1/$page.gz
  done
)

# Compress info files, if any:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

mkdir -p $PKG/usr/doc/bash-$VERSION
cp -a AUTHORS CHANGES COMPAT COPYING INSTALL MANIFEST NEWS NOTES \
  README Y2K doc/FAQ doc/INTRO \
  $PKG/usr/doc/bash-$VERSION
( cd doc ; groff -ms -Tascii article.ms > $PKG/usr/doc/bash-$VERSION/article.txt )

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/*-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

# Build the package:

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
